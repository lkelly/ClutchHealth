import React from 'react';
import styles from './css/banner.css';

var Banner = React.createClass({
  propTypes: {},
  render: function() {
    return <div>

      <div className={styles['hero']}>
        {/* <img src="https://i.ytimg.com/vi/0nnzO0k5rP0/maxresdefault.jpg"/> */}
        <h1>START YOUR PATH TO GREATNESS</h1>
        <p>Winning results that help you succeed.</p>
        <button className={styles['btn-primary']}>Test</button>

      </div>
    </div>
  }
});

export default Banner;
