import React, { Component, PropTypes } from 'react';
    import { connect } from 'react-redux';
    import { bindActionCreators } from 'redux';

    //component imports
    import Banner from './../components/Banner';

    // Import Style
    // import './Test.css';

    class TestPage extends Component {
      render() {
        return (
          <div style={{height: "1000px"}}>
          <Banner />
        </div>
        );
      }
    }

    const mapStateToProps = (state) => {
      return {};
    };

    const mapDispatchToProps = (dispatch) => {
      return {};
    };

    TestPage.propTypes = {
    };

    export default connect(
      mapStateToProps,
      mapDispatchToProps
    )(TestPage);
